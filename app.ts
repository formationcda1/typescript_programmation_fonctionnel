//import readline
const readline = require("readline")
//readline.createInterface() est utilisé pour créer une instance de readline en configurant les flux de lecture et d'écriture
const rl = readline.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
)

/*/ 1. Convertir devises 
1 EUR = 1.5 CAD = 130 JPY
1 CAD = 0.67 EUR = 87 JPY
1 JPY = 0.0077 EUR = 0.0115 CAD


JE VEUT POUVOIR ENTRER DES NOMBRES 
JE VEUT QUE LE RESULTAT ME RETOURNE UN NOMBRE CONVERTIE
Comment ? faire une fonction qui va convertir le montant vers une autre devise
entrées : montant, deviseDuMontant, deviseSortante
si EUR -> EUR * 1.5 = CAD ou si EUR -> EUR * 130 = JPY 
si CAD -> CAD * 0.67 = EUR ou si CAD -> CAD * 87 = JPY
si JPY -> JPY * 0.0077 = EUR ou si JPY -> JPY * 0.0115 = CAD


 if (deviseDuMontant = 'JPY' && deviseSortante = 'CAD') {
  return  `${montant * 0.0115} CAD`
}
/*/
// il est mieux d'avoir une fonction qui ne retourne qu'un seule type ici : number , il faut integrer les erreurs à chaque conditon 
function conversion(montant: number, deviseDuMontant: string, deviseSortante: string) {
    if (deviseDuMontant === 'JPY' && deviseSortante === 'CAD') {
        return `${montant * 0.0115} CAD`

    } else if (deviseDuMontant === 'JPY' && deviseSortante === 'EUR') {
        return `${montant * 0.0077} EUR`

    } else if (deviseDuMontant === 'EUR' && deviseSortante === 'CAD') {
        return `${montant * 1.5} CAD`

    } else if (deviseDuMontant === 'EUR' && deviseSortante === 'JPY') {
        return `${montant * 130} JPY`

    } else if (deviseDuMontant === 'CAD' && deviseSortante === 'EUR') {
        return `${montant * 0.67} EUR`

    } else if (deviseDuMontant === 'CAD' && deviseSortante === 'JPY') {
        return `${montant * 87} JPY`
    }
    else {
        return "Valeurs non prises en charge."
    }
}

//console.log(conversion(50, 'EUR', 'CAD'))

/*/2. Calculateur de frais de livraison

//nom de la fonction fraisDeLivraison
// entrée : kg, longueur, largeur, hauteur en cm, pays de destination
// sortie : Retourne les frais de livraison dans la devise correspondant au pays de destination

//si kg < 1 kg && pays de destination = 'EUR' alors + 10EUR 
//si kg entre 1 et 3 kg && pays de destination = 'EUR' alors + 20EUR = 
//si kg > 3 kg && pays de destination = 'EUR' alors + 30EUR = 
//si somme dimensions colis depasse 150 cm = supplement 5 EUR, 7.5 CAD, 500 JPY

Jusqu'à 1 kg : 10 EUR, 15 CAD, 1000 JPY
Entre 1 et 3 kg : 20 EUR, 30 CAD, 2000 JPY
Plus de 3 kg : 30 EUR, 45 CAD, 3000 JPY/*/

/*function envoiColis(kg: number, longueur: number, largeur: number, hauteur: number, destination: string) {
    let fraisDeLivraison = 0;
    let supplement = 0;
    let prixTotal = fraisDeLivraison + supplement;
   
        {
            if (kg < 1) {
                if (destination === 'EUR') {
                    fraisDeLivraison += 10
                    if (longueur + largeur + hauteur > 150){
                    supplement += 5
                    }
                } else if (destination === 'CAD') {
                    fraisDeLivraison += 15
                } else if (destination === 'JPY') {
                    fraisDeLivraison += 1000
                } else {
                    return 'Destination non prise en charge'        // il faudrait declarer des erreurs
                }
            } else if (kg >= 1 && kg <= 3) {
                if (destination === 'EUR') {
                    fraisDeLivraison += 25
                    if (longueur + largeur + hauteur > 150){
                        supplement += 7.50
                        }
                } else if (destination === 'CAD') {
                    fraisDeLivraison += 30
                } else if (destination === 'JPY') {
                    fraisDeLivraison += 2000
                } else {
                    return 'Destination non prise en charge'
                }
            } else if (kg > 3) {
                if (destination === 'EUR') {
                    fraisDeLivraison += 30
                    if (longueur + largeur + hauteur > 150){
                        supplement += 500
                        }
                } else if (destination === 'CAD') {
                    fraisDeLivraison += 45
                } else if (destination === 'JPY') {
                    fraisDeLivraison += 3000
                } else {
                    return 'Destination non prise en charge'
                }
            }
        }
    
    
   /* if (longueur + largeur + hauteur > 150) {
        if (destination === 'EUR') {
            supplement = 5;
        } else if (destination === 'CAD') {             //j'aurais pu faire une fonctionne specifique au supplement
            supplement = 7.50;
        } else if (destination === 'JPY') {
            supplement = 500;
            console.log("coucou je suis passé par le supplement 500")
        } else {
            return 'Destination non prise en charge'
        }
     }
         */

/*return `Vos frais de livraison s'élèvent à ${fraisDeLivraison} ${destination} . Le prix total est de ${prixTotal}${destination}`;
} */
//console.log(envoiColis(3, 300, 4, 5, 'EUR'));


function envoiColis(kg: number, longueur: number, largeur: number, hauteur: number, destination: string) {
    let supplement = 0;
    if (longueur + largeur + hauteur > 150) {
        if (destination === 'EUR') {
            supplement = 5;
        } else if (destination === 'CAD') {             //j'aurais pu faire une fonctionne specifique au supplement
            supplement = 7.50;
        } else if (destination === 'JPY') {
            supplement = 500;
            console.log("coucou je suis passé par le supplement 500")
        } else {
            return 'Destination non prise en charge'
        }
    }
    let fraisDeLivraison = 0;
    {
        if (kg < 1) {
            if (destination === 'EUR') {
                fraisDeLivraison += 10
            } else if (destination === 'CAD') {
                fraisDeLivraison += 15
            } else if (destination === 'JPY') {
                fraisDeLivraison += 1000
            } else {
                return 'Destination non prise en charge'        // il faudrait declarer des erreurs
            }
        } else if (kg >= 1 && kg <= 3) {
            if (destination === 'EUR') {
                fraisDeLivraison += 25
            } else if (destination === 'CAD') {
                fraisDeLivraison += 30
            } else if (destination === 'JPY') {
                fraisDeLivraison += 2000
            } else {
                return 'Destination non prise en charge'
            }
        } else if (kg > 3) {
            if (destination === 'EUR') {
                fraisDeLivraison += 30
            } else if (destination === 'CAD') {
                fraisDeLivraison += 45
            } else if (destination === 'JPY') {
                fraisDeLivraison += 3000
            } else {
                return 'Destination non prise en charge'
            }
        }
    }
    fraisDeLivraison + supplement

    return ` ${fraisDeLivraison + supplement} ${destination} .`;
}
/*
3. Calcul de frais de douanes
 
L'utilisateur doit pouvoir calculer les frais de douane basés sur la valeur déclarée du colis et le pays de destination (l'origine des colis sera toujours la France).
 
Pour le Canada, 15 % de taxes pour les envois de plus de 20 CAD
Pour le Japon, 10 % de taxes pour les envois de plus de 5000 JPY
Pas de taxes supplémentaires pour la France.
 
 
Retourne les frais de douane dans la devise correspondant au pays de destination.
*/
/*
nom de la fonction fraisDeDouane
entrée: valeur du colis (number), pays de destination(string)
sortie : somme du  prix de l'envoi + frais de douane (number)
 
si pays de destination = Canada -> valeur du colis > 20CAD alors + 15% de valeur du colis
si pays de destination = Japon -> valeur du colis  > 5000JPY alors + 10% de valeur du colis
si pays de destination = France -> pas de frais de douane
 
*/
function fraisDeDouane(colisValue: number, destination: string) {
    let douane = 0;
    if (destination === 'Canada') {
        if (colisValue > 20) {
            douane = colisValue * 0.15
        }
    } else if (destination === 'Japon') {
        if (colisValue > 5000) {
            douane = colisValue * 0.1
        }

    } else if (destination === 'France') {
        return `Frais de douane : ${douane}EUR. Pour un envoi vers la France vous n'avez pas de frais de douane a payer.`
    } else {
        return "Valeurs non prises en charge"
    }
    return `Frais de douane : ${douane} ${destination === 'Japon' ? 'JPY' : 'CAD'}.`;
}

//console.log(fraisDeDouane(9000, 'Canada'))

// Validations readlines

rl.question("Quel est le montant à convertir ?", function (montant: number) {
    if (montant < 0) {
        throw new Error('Attention la valeur renseigné est négative')
    }
    rl.question("Quel est la devise du montant ?", function (deviseDuMontant: string) {
        rl.question("Dans quelle devise voulez vous effectuer la conversion?", function (deviseSortante: string) {
            console.log(conversion(montant, deviseDuMontant, deviseSortante))
            rl.question("Veuillez renseigner le poid en kg du colis :", function (kg: number) {
                if (kg > 150) {
                    throw new Error('Le poid du colis est trop important')
                }
                rl.question("Veuillez renseigner la longueur de votre colis en cm: ", function (longeur: number) {
                    rl.question("Veuillez renseigner la largeur de votre colis en cm: ", function (largeur: number) {
                        rl.question("Veuillez renseigner la hauteur de votre colis en cm: ", function (hauteur: number) {
                            rl.question("Veuillez renseigner la destination du colis EUR, CAD ou JPY : ", function (destination: string) {
                                console.log(envoiColis(kg, longeur, largeur, hauteur, destination))
                                rl.question("Renseignez le prix du colis :", function (colisValue: number) {
                                    if (colisValue < 0) {
                                        throw new Error("La valeur du colis ne peut être négative")
                                    } rl.question("Veuillez renseigner la destination du colis", function (destination: string) {
                                        console.log(fraisDeDouane(colisValue, destination))
                                        rl.close()
                                    }
                                    )
                                }
                                )
                            }
                            )
                        })
                    })
                })
            })
        }
        )
    })
})