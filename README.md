# INSTALLATION DE TYPESCRIPT
-Initialiser npm ```npm init -y```
-Initialiser un projet avec Node ```npm install nodemon -D```
-Installer le package TypeScript avec la commande ```npm install -g typescript```
-Lancer une initialisation d'un nouveau projet TypeScript avec la commande ``` tsc -- init``` cette commande crée un fichier ```tsconfig.json``` qui me permettra de chosir les option de compilation de mon fichier
-Je crée un fichier TypeScript ```app.ts```
-Je compile mon fichier'app.ts' en fichier javaScript ```app.js``` avec la commande ```npx tsc app.ts```
-```npm run dev```



